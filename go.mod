module library-api

go 1.14

require (
	github.com/friendsofgo/errors v0.9.2
	github.com/go-sql-driver/mysql v1.5.0
	github.com/kat-co/vala v0.0.0-20170210184112-42e1d8b61f12
	github.com/labstack/echo v3.3.10+incompatible
	github.com/labstack/echo/v4 v4.1.16
	github.com/lib/pq v1.2.1-0.20191011153232-f91d3411e481
	github.com/spf13/viper v1.6.3
	github.com/volatiletech/null/v8 v8.1.0
	github.com/volatiletech/randomize v0.0.1
	github.com/volatiletech/sqlboiler v3.7.1+incompatible
	github.com/volatiletech/sqlboiler/v4 v4.2.0
	github.com/volatiletech/strmangle v0.0.1
)
