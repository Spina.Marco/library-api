package controller

import (
	"fmt"
	"library-api/database"
	"library-api/models"
	"net/http"
	"strconv"

	"github.com/labstack/echo/v4"
)

func sendError(err error) error {
	return echo.NewHTTPError(http.StatusInternalServerError, fmt.Sprintln("an error has occured: ", err))
}

//GetBooks returns a json of the books slice
func GetBooks(c echo.Context) error {
	books, err := database.GetBooks()

	if err != nil {
		return sendError(err)
	}

	return c.JSON(http.StatusOK, books)
}

//GetBook returns the book with the specifed id as a JSON, if there is an error returns an error instead
func GetBook(c echo.Context) error {
	id, _ := strconv.Atoi(c.Param("id"))
	book, err := database.GetBook(id)

	if err != nil {
		return sendError(err)
	}

	return c.JSON(http.StatusOK, book)
}

//AddBook ...
func AddBook(c echo.Context) error {
	var book models.Book
	var err error

	book.Title = c.FormValue("title")

	if book.Title == "" {
		return echo.NewHTTPError(http.StatusBadRequest, "please input a non-empty title")
	}

	book, err = database.AddBook(book)

	if err != nil {
		return sendError(err)
	}

	return c.JSON(http.StatusOK, book)
}

//DeleteBook gets a book from the database using the id parameter and then calls database.DeleteBooks to remove the specified book from the database.
//returns the number of rows affected unless there is an error, in wich case returns the error message
func DeleteBook(c echo.Context) error {
	id, _ := strconv.Atoi(c.Param("id"))
	book, err := database.GetBook(id)

	if err != nil {
		return sendError(err)
	}

	rowAffected, err := database.DeleteBook(*book)

	if err != nil {
		return sendError(err)
	}

	return c.JSON(http.StatusOK, rowAffected)
}

//UpdateBook ...
func UpdateBook(c echo.Context) error {
	id, _ := strconv.Atoi(c.Param("id"))
	book, err := database.GetBook(id)

	if err != nil {
		return sendError(err)
	}

	book.Title = c.FormValue("title")

	if book.Title == "" {
		return echo.NewHTTPError(http.StatusBadRequest, "please input a non-empty title")
	}

	rowAffected, err := database.UpdateBook(*book)

	if err != nil {
		return sendError(err)
	}

	return c.JSON(http.StatusOK, rowAffected)
}
