package database

import (
	"context"
	"database/sql"
	"library-api/driver"
	"library-api/models"

	"github.com/volatiletech/sqlboiler/v4/boil"
)

var db *sql.DB

//opens a connection to the database
func init() {
	db = driver.ConnectToDatabase()
}

//GetBooks performs a select * for the book table in the database
func GetBooks() (models.BookSlice, error) {

	books, err := models.Books().All(context.Background(), db)

	return books, err
}

//GetBook returns the book with the specified id
func GetBook(id int) (*models.Book, error) {

	book, err := models.FindBook(context.Background(), db, id)

	return book, err
}

//AddBook performs an insert of the book object in the book table of the database
func AddBook(b models.Book) (models.Book, error) {

	err := b.Insert(context.Background(), db, boil.Infer())

	return b, err
}

//DeleteBook deletes a specified book from the database and returns the affected row
func DeleteBook(b models.Book) (rowAffected int64, err error) {
	rowAffected, err = b.Delete(context.Background(), db)

	return rowAffected, err
}

//UpdateBook updates a specified book from the database and returns the affected row
func UpdateBook(b models.Book) (rowsAffected int64, err error) {
	rowsAffected, err = b.Update(context.Background(), db, boil.Infer())

	return rowsAffected, err
}
