package main

import (
	"library-api/controller"

	"github.com/labstack/echo/v4"
	"github.com/labstack/echo/v4/middleware"
	_ "github.com/volatiletech/sqlboiler/v4/boil"
)

func main() {

	e := echo.New()

	e.Use(middleware.CORS())

	e.GET("/book", controller.GetBooks)
	e.GET("/book/:id", controller.GetBook)
	e.POST("/book", controller.AddBook)
	e.DELETE("/book/:id", controller.DeleteBook)
	e.PUT("/book/:id", controller.UpdateBook)

	e.Logger.Fatal(e.Start(":8000"))

}
