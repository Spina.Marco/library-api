package driver

import (
	"database/sql"
	"fmt"

	//import for psql driver
	_ "github.com/lib/pq"
)

//ConnectToDatabase opens a connection to the mysql database
func ConnectToDatabase() *sql.DB {

	db, err := sql.Open("postgres", "dbname=library user=postgres password=password host=localhost port=5432 sslmode=disable")
	CheckForErrors(err)

	err = db.Ping()
	CheckForErrors(err)

	fmt.Println("connected to database")

	return db

}

//CheckForErrors panics if err is not nil
func CheckForErrors(err error) {
	if err != nil {
		panic(err)
	}
}
